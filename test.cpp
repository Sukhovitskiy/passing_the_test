#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <test.h>
void Test::serialization() {
    std::ofstream fout("data.txt");
    fout << data.size() << std::endl;
    for (int i = 0; i < data.size(); i++) {
        fout << data[i].que << std::endl;
        fout << data[i].correct_ans << std::endl;
        fout << data[i].ans1 << std::endl;
        fout << data[i].ans2 << std::endl;
        fout << data[i].grade << std::endl;
    }
    fout.close();
}
void Test::deserialization(std::string path) {
    std::ifstream fin(path);
    if(fin.peek() != EOF){
        int b;
        std::string a;
        getline(fin, a);
        try{
        b = std::stoi(a);//возможен инвалид аргумент
        }
        catch(std::invalid_argument){
            throw std::invalid_argument("First line of the file has to be an int value. It is count of questions");
        }
        data.resize(b);
        for (int i = 0; i < data.size(); i++) {
            getline(fin, data[i].que);
            getline(fin, data[i].correct_ans);
            getline(fin, data[i].ans1);
            getline(fin, data[i].ans2);
            getline(fin, data[i].grade);
        }
        fin.close();
    }
}
int Test::cout_questions(){
    return data.size();
}

std::vector<std::string> Test::at(int i){
    std::vector<std::string> arr(5);
    arr[0] = data[i].que;
    arr[1] = data[i].correct_ans;
    arr[2] = data[i].ans1;
    arr[3] = data[i].ans2;
    arr[4] = data[i].grade;
    return arr;
}

void Test::print() {
    for (int i = 0; i < data.size(); i++) {
        std::cout << data[i].que << std::endl;
        std::cout << "1)" << data[i].correct_ans << std::endl;
        std::cout << "2)" << data[i].ans1 << std::endl;
        std::cout << "3)" << data[i].ans2 << std::endl;
    }
}
void Test::add_question(std::string question, std::string correct, std::string ans1, std::string ans2, std::string grade) {
    Question que;
    que.que = question;
    que.correct_ans = correct;
    que.ans1 = ans1;
    que.ans2 = ans2;
    que.grade = grade;
    data.push_back(que);
}
std::string Test::read_entity() {
    std::string buff, line;
    do
    {
        if (line.size() != 0) {
            buff += line;
            buff += " ";
        }
        std::getline(std::cin, line);
    } while (line != "EOL");
    return buff;
}
