#include "fill_question.h"
#include "ui_fill_question.h"
#include "QMessageBox"
#include "test.h"
fill_question::fill_question(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::fill_question)
{
    ui->setupUi(this);
}

fill_question::~fill_question()
{
    delete ui;
}

void fill_question::on_pushButton_clicked()
{
    QMessageBox *ms = new QMessageBox;
    Test test;
    try{
        test.deserialization("data.txt");
    }catch(std::invalid_argument& exeption){
        QString val = exeption.what();
        ms->setText(val);
        ms->exec();
        return;
    }
    QString str1 = ui->lineEdit1->text();
    QString str2 = ui->lineEdit2->text();
    QString str3 = ui->lineEdit3->text();
    QString str4 = ui->lineEdit4->text();
    QString str5 = ui->lineEdit5->text();
    if(str1 == "" || str2 == "" || str3 == "" || str4 == "" || str5 == ""){
        ms->setText("Please, fill all lines");
        ms->exec();
        return;
    }
    if(!(str5 == "Easy" || str5 == "Medium" || str5 == "Hard")){
        ms->setText("You have to input 'Easy', 'Medium', 'Hard'");
        ms->exec();
        return;
    }
    test.add_question(str1.toStdString(), str2.toStdString(), str3.toStdString(), str4.toStdString(), str5.toStdString());
    test.serialization();
    ms->setText("Вопрос сохранен");
    ms->exec();
    ui->lineEdit1->setText("");
    ui->lineEdit2->setText("");
    ui->lineEdit3->setText("");
    ui->lineEdit4->setText("");
    ui->lineEdit5->setText("");
}

