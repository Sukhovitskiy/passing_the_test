#ifndef COMPLETE_TEST_H
#define COMPLETE_TEST_H

#include <QDialog>
#include "test.h"

namespace Ui {
class Complete_test;
}

class Complete_test : public QDialog
{
    Q_OBJECT

public:
    explicit Complete_test(QWidget *parent = nullptr);
    ~Complete_test();

private slots:

    void on_start_test_button_clicked();

    void on_finish_test_button_clicked();

    void on_check_answer_button_clicked();

    void on_go_to_next_button_clicked();

private:
    Ui::Complete_test *ui;
    struct start{
        int count_correct_answers;
        int count_questions;
        std::vector<std::string> current_question;
        std::vector<QString> qstr;
        std::string difficulty;
        Test test;
    };
    start obj;
    void choose_difficulty();
};

#endif // COMPLETE_TEST_H
