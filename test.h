#ifndef KURSACH
#define KURSACH
#include <iosfwd>
#include <fstream>
#include <string>
#include <vector>
class Test {
public:
    Test() = default;
    Test(const Test&) = default;
    Test& operator=(const Test&) = default;
    ~Test() = default;
    void serialization();
    void deserialization(std::string path);
    void print();
    int cout_questions();
    void add_question(std::string question, std::string correct, std::string ans1, std::string ans2, std::string grade);
    std::vector<std::string> at(int i);
private:
    std::string read_entity();
    struct Question {
        std::string que;
        std::string correct_ans;
        std::string ans1;
        std::string ans2;
        std::string grade;
    };
    std::vector<Question> data;
};
#endif

