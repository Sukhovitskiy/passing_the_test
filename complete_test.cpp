#include <string>
#include <ctime>
#include "complete_test.h"
#include "ui_complete_test.h"
#include "test.h"
#include "QMessageBox"


Complete_test::Complete_test(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Complete_test)
{
    obj.count_correct_answers=0;
    obj.count_questions = 0;
    ui->setupUi(this);
    ui->textEdit->hide();
    ui->finish_test_button->hide();
    ui->label_2->hide();
    ui->label_3->hide();
    ui->lineEdit_2->hide();
    ui->textEdit_2->hide();
    ui->check_answer_button->hide();
    ui->go_to_next_button->hide();
}

Complete_test::~Complete_test()
{
    delete ui;
}

void Complete_test::choose_difficulty(){
    bool flag = true;
    srand(time(NULL));
    while(flag)
    {
         obj.current_question = obj.test.at(rand()%obj.test.cout_questions());
         if(obj.current_question[4] == obj.difficulty){
            flag = false;
         }
    }
}

void Complete_test::on_start_test_button_clicked()
{
    QMessageBox *ms = new QMessageBox;
    Test test;
    try{
        obj.test.deserialization("data.txt");
    }catch(std::invalid_argument& exeption){
        QString val = exeption.what();
        ms->setText(val);
        ms->exec();
        return;
    }
    if( ui->lineEdit->text().toStdString() == "Easy" || ui->lineEdit->text().toStdString() == "Medium" ||  ui->lineEdit->text().toStdString() == "Hard"){
        obj.difficulty = ui->lineEdit->text().toStdString();
    }
    else{
        ms->setText("Enter 'Easy', 'Medium', 'Hard' ");
        ms->exec();
        return;
    }
    ui->label->hide();
    ui->lineEdit->hide();
    ui->start_test_button->hide();
    ui->textEdit->hide();
    ui->textEdit_2->show();
    ui->label_2->show();
    ui->label_3->show();
    ui->check_answer_button->show();
    ui->lineEdit_2->show();
    ui->lineEdit_2->setText("");
    obj.count_questions++;
    if(obj.test.cout_questions() == 0){
        ms->setText("Список вопросов пуст");
        ms->exec();
        return;
    }
    ui->label_2->setText("Вопрос № " + QString::number(obj.count_questions));
    choose_difficulty();
    obj.qstr.resize(0);
    std::vector<int> index;
    int j = 0;
    obj.qstr.push_back(QString::fromStdString(obj.current_question[0]));
    for(int i = 0; i < 3;){
        j = rand()%3 + 1;
        if(std::find(index.begin(), index.end(), j) == index.end()){
            index.push_back(j);
            obj.qstr.push_back(QString::fromStdString(obj.current_question[j]));
            i++;
        }
    }
    QString qw = obj.qstr[0] + '\n' + '1' + ')' + ' '+ obj.qstr[1] + '\n' + '2' + ')' + ' ' + obj.qstr[2] + '\n' + '3'+ ')' + ' ' + obj.qstr[3];
    ui->textEdit_2->setText(qw);

}


void Complete_test::on_finish_test_button_clicked()
{
    ui->textEdit->show();
    QString s = "Тестирование окончено.";
    QString s1 = "Количество решенных вопросов: ";
    QString s2 = "Количество правильно решенных вопросов: ";
    QString qw =  s  + '\n' + s1 + QString::number(obj.count_questions) + '\n' + s2 + QString::number(obj.count_correct_answers);
    obj.count_questions = 0;
    obj.count_correct_answers = 0;
    ui->textEdit->setText(qw);
    ui->go_to_next_button->hide();
    ui->start_test_button->show();
    ui->finish_test_button->hide();
}


void Complete_test::on_check_answer_button_clicked()
{

    QMessageBox *ms = new QMessageBox;
    int val;
    val = ui->lineEdit_2->text().toInt();
    if(!(val == 1 || val == 2 || val == 3)){
        ms->setText("please, input 1 or 2 or 3");
        ms->exec();
        return;
    }
    ui->go_to_next_button->show();
    ui->check_answer_button->hide();
    ui->finish_test_button->show();
    if(obj.qstr[val].toStdString() == obj.current_question[1]){
        ms->setText("Correct!!!");
        obj.count_correct_answers++;
    }

    else{
        ms->setText("Wrong answer!!!");
    }
    ms->show();
}


void Complete_test::on_go_to_next_button_clicked()
{
    ui->lineEdit_2->setText("");
    ui->check_answer_button->show();
    ui->go_to_next_button->hide();
    ui->textEdit->hide();
    obj.count_questions++;
    ui->label_2->setText("Вопрос № " + QString::number(obj.count_questions));
    choose_difficulty();
    obj.qstr.resize(0);
    std::vector<int> index;
    int j = 0;
    obj.qstr.push_back(QString::fromStdString(obj.current_question[0]));
    for(int i = 0; i < 3;){
        j = rand()%3 + 1;
        if(std::find(index.begin(), index.end(), j) == index.end()){
            index.push_back(j);
            obj.qstr.push_back(QString::fromStdString(obj.current_question[j]));
            i++;
        }
    }
    QString qw = obj.qstr[0] + '\n' + '1' + ')' + ' '+ obj.qstr[1] + '\n' + '2' + ')' + ' ' + obj.qstr[2] + '\n' + '3'+ ')' + ' ' + obj.qstr[3];
    ui->textEdit_2->setText(qw);
    ui->lineEdit_2->setText("");
}

