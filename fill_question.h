#ifndef FILL_QUESTION_H
#define FILL_QUESTION_H

#include <QDialog>

namespace Ui {
class fill_question;
}

class fill_question : public QDialog
{
    Q_OBJECT

public:
    explicit fill_question(QWidget *parent = nullptr);
    ~fill_question();

private slots:
    void on_pushButton_clicked();

private:
    Ui::fill_question *ui;
};

#endif // FILL_QUESTION_H
