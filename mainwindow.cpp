#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "fill_question.h"
#include "complete_test.h"
#include "test.h"
#include <vector>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_add_question_clicked()
{
    fill_question* question = new fill_question;
    question->show();

}


void MainWindow::on_test_button_clicked()
{
    Complete_test* complete_test = new Complete_test;
    complete_test->show();
}

